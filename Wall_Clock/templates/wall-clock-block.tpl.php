 <?php 
/**
 * @file
 * Show beautifull clock custom block in Drupal sites.
 */
?>
<div class="clock">
    <canvas style="width: 170px; height: 170px;" height="170" width="170" id='clockId' class="wallClock:<?php echo $clock;?>::<?php echo ($second_hand) ? 'noSeconds' : ''?>:<?php echo ($time_zone) ? $time_zone : ''?>:<?php echo ($digital_clock) ? 'showDigital' : ''?>:"></canvas>
</div>
