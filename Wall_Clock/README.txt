CONTENTS OF THIS FILE
---------------------

 * Summary
 * Requirements
 * Installation
 * Cofiguration
 * Contact

 -- SUMMARY --

 The Wall cloclk module provieds you to choose different beautifull clock
 dials and show it in a wall clock block.It provides you a functionality to
 add second han, digital clock and select different Time Zones.


 -- REQUIREMENTS --

 None.

 -- INSTALLATION --

 To install Wall Clock:

 * Download the latest version of the Wall Clock and extract it to 
   sites\all\modules . 
 * Enable Wall Clock.
 *Configure Wall Clock as required.
 *Now goto Structure->Block and place the wall Clock block as required.



 -- CONFIGURATION --

* Choose different wall Clock dials by select the radio button.
* Check the "Don't Show second hand" checkbox if you don't want second hand 
  in the wall clock.
* Check the "Show digital clock" checkbox if you want to display digital clock 
  in Wall clock.
* Select Time Zone as required from the drop down list.


 -- CONTACT --

Current maintainers:
* Mohit K. Singh (Mohit_Singh) - http://drupal.org/user/
